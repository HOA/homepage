class RelativeMediaUrls:
    def media_added(self, context, content, media):
        media["url"] = f"{context.settings.MEDIA_PREFIX}{media['url']}"
