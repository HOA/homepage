import datetime
import os
import json
import flamingo
from flamingo.core.context import Context
from flamingo.core.data_model import Content, ContentSet
from flamingo.core.templating import TemplatingEngine

# Event Core Infos
EVENT_INFO = {
    "next": {
        "start": "2025-07-01",
        "end": "2025-07-05",
        "event": "Hacken Open Air 2::25",
        "location_city": "Gifhorn",
    }
}

THEME_PATHS = [
        'theme/',
]

PLUGINS = [
    'flamingo.plugins.Redirects',
    'flamingo.plugins.rstPygments',
    'plugins/title.py::Title',
    'plugins/relative_media_urls.py::RelativeMediaUrls',
    'plugins/thumbnails.py::Thumbnails',
]

MEDIA_PREFIX = "."

HTML_PARSER_RAW_HTML = True

@flamingo.hook("contents_parsed")
def fix_headings(context):
    for content in context.contents:
        if os.path.splitext(content["path"])[-1] == ".html":
            continue
        cb = content["content_body"]
        cb = cb.replace("h3>", "h4>")
        cb = cb.replace("h2>", "h3>")
        content["content_body"] = cb

POST_BUILD_LAYERS = [
        'downloads',
]

@flamingo.hook("templating_engine_setup")
def event_info_templates(_context: Context, templating_engine: TemplatingEngine):
    """Add the Event-Info to the template context."""

    start = datetime.date.fromisoformat(EVENT_INFO["next"]["start"])
    end = datetime.date.fromisoformat(EVENT_INFO["next"]["end"])

    templating_engine.env.globals["EVENT_HEADLINE"] = EVENT_INFO["next"]["event"]
    templating_engine.env.globals["EVENT_TAGLINE"] = \
        f"{start.strftime('%d.%m.')} - {end.strftime('%d.%m.%Y')} in {EVENT_INFO['next']['location_city']}"

@flamingo.hook("contents_parsed")
def event_info_json(context: Context):
    """Write the event info as infos.json into the output."""
    contents: ContentSet = context.contents
    contents.add(Content(output="infos.json", content_body=json.dumps(EVENT_INFO), path=""))
