PYTHON=python3
PYTHON_VENV=env

PROJECT_ROOT=.
WEBSERVER_PORT=8080
WEBSERVER_HOST=0.0.0.0

FLAMINGO_OUTPUT=output
FLAMINGO_ARGS=-s settings.py
FLAMINGO_SERVER_ARGS=--port=$(WEBSERVER_PORT) --host=$(WEBSERVER_HOST) -s settings.py settings-server.py

all: server

# setup / clean ###############################################################
$(PYTHON_VENV)/.created.deploy:
	rm -rf $(PYTHON_VENV) && \
	$(PYTHON) -m venv $(PYTHON_VENV) && \
	. $(PYTHON_VENV)/bin/activate && \
        pip install pip --upgrade && \
	pip install --upgrade -r ./REQUIREMENTS-deploy.txt > $(PYTHON_VENV)/build.log 2>&1 && \
	(date > $(PYTHON_VENV)/.created.deploy) || \
	(echo "\e[31m"; cat $(PYTHON_VENV)/build.log; echo "\e[0m"; exit 1)

deployenv: $(PYTHON_VENV)/.created.deploy

$(PYTHON_VENV)/.created.full:
	. $(PYTHON_VENV)/bin/activate && \
        pip install pip --upgrade && \
	pip install --upgrade -r ./REQUIREMENTS-full.txt > $(PYTHON_VENV)/build.log 2>&1 && \
	(date > $(PYTHON_VENV)/.created.full) || \
	(echo "\e[31m"; cat $(PYTHON_VENV)/build.log; echo "\e[0m"; exit 1)

env: deployenv $(PYTHON_VENV)/.created.full

clean:
	rm -rf $(FLAMINGO_OUTPUT)

distclean:
	rm -rf $(PYTHON_VENV)


# build #######################################################################

sass: deployenv
	. $(PYTHON_VENV)/bin/activate && \
	mkdir -p theme/static/css/ && \
	pysassc -t compressed theme/sass/*.scss theme/static/css/hoa.css

html: deployenv sass
	. $(PYTHON_VENV)/bin/activate && \
	flamingo build $(FLAMINGO_ARGS)

server: env sass
	. $(PYTHON_VENV)/bin/activate && \
	flamingo server $(FLAMINGO_SERVER_ARGS)

shell: env sass
	. $(PYTHON_VENV)/bin/activate && \
	flamingo shell $(FLAMINGO_ARGS)
