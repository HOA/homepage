menu: Top
group: index
order: 1
template: page.html


Titel
-----

Das HOA25 findet wieder von Dienstag bis Samstag (01. - 05.07.2025) statt.
Tickets gibt es unter `tickets.hackenopenair.de <https://tickets.hackenopenair.de>`__.
Alle weiteren Infos werden wir nach und nach hier und natürlich auf unserem
`Mastodon <https://chaos.social/@HackenOpenAir>`__
veröffentlichen.

Diese Sneak-Peak aus der Orga wollen wir euch noch mitgeben:
In diesem Jahr wollen wir noch mehr Chaos wagen.
Also mehr Raum für eure Ideen und eure Eskalationen!

.. raw:: html

   <hr>

Das Hacken Open Air ist feinstes Hacker-Camping, ausgerichtet vom `Stratum0 <https://stratum0.org>`__.
Gemeinsam mit euch wollen wir in der Natur tüfteln, diskutieren und kreativ sein.

Die Location ist wieder das Pfadfinderheim Welfenhof in Gifhorn, ganz in der Nähe von Braunschweig. 
Adresse::

    Pfadfinderheim Welfenhof
    III. Koppelweg 6
    38518 Gifhorn

Wir freuen uns auf euch!

Infrastruktur
=============

Der Campingplatz verfügt über feste Indoor-Duschen und Toiletten, dazu bauen wir wieder Open-Air-Duschen auf.
Zusätzlich bieten wir flächendeckend Strom, DECT und Wi-Fi an, sodass Ihr perfekt zum Hacken und Chillen ausgestattet
seid.
Für die Erholung wird es aller Voraussicht nach wieder einen Pool mit Sauna sowie ein direkt angrenzendes Lagerfeuer
geben.
Dort könnt ihr den Abend mit Getränken an der Bar ausklingen lassen.

(H)Elfen
========

Als Community-Event würde es das HOA ohne die Partizipation der Teilnehmenden gar nicht geben.
Wie jedes Jahr gibt es in der Vorbereitung, während des Events und beim Abbau jede Menge Dinge zu tun.

Kunst und Deko
==============

Wir lieben bunte Lichter und würden uns freuen, wenn das HOA25 auch wieder so wunderhübsch wird wie das vorangegangene
HOA.
Wer Ideen für Deko oder Kunstinstallationen hat, ist herzlich eingeladen sich (wieder) einzubringen.

Weitere Fragen zum Thema Kunst und Deko beantworten wir gerne unter
`kontakt@hackenopenair.de <mailto:kontakt@hackenopenair.de>`__.

Essen
=====

Es gibt jeden Tag zwischen 09.00 und 12.00 ein Frühstücksbuffet.
Dazu kochen wir jeden Tag ein Abendessen.

Frühstück und Abendessen während der Veranstaltung sind im Ticketpreis enthalten.
Zwischendurch werfen wir planmäßig auch wieder die Fritteuse an.

Wir haben Besteck, Geschirr und Tassen, die wir selber spülen, es muss daher nichts davon mitgebracht werden.

Campen
======

Pro Person stehen auf dem Zeltplatz circa 12 m² zur Verfügung.
Beim Strom planen wir mit maximal 100 W pro Person auf der Campingfläche.
Solltest du größere Verbraucher dabei haben, gib uns bitte Bescheid.


Tagestickets
============

Für alle Tage (Dienstag bis Freitag) gibt es Tagestickets an der Tageskasse.
Dieses berechtigt das HOA ab 09:00 bis zur Nacht zu besuchen.
Ein Abendessen ist inklusive.
Das Ticket berechtigt nicht zur Übernachtung.



.. image:: faehnchen.jpeg
   :width: 600px
   :title: Viele bunte Faehnchen an einer Schnur an einem hohen Mast.

.. image:: leuchtebunt.jpeg
   :width: 600px
   :title: Viele Zelte mit bunten Lichtern bei Nacht.

.. image:: pool.jpeg
   :width: 600px
   :title: Pool mit schwimmendem Einhorn, Lastenrad und Lagerfeuer davor mit bunten Lichtern bei Nacht.

