menu: Top
group: en
order: 1
template: page.html


Topic
-----

HOA25 will take place again from Tuesday to Saturday (July 1st - 5th, 2025).
Tickets are available at `tickets.hackenopenair.de <https://tickets.hackenopenair.de>`__.
We will publish all further information here and of course on our
`Mastodon <https://chaos.social/@HackenOpenAir>`__.


We would like to share this sneak peak with you:
This year we want to dare to create even more chaos.
So more room for your ideas and your escalations!

.. raw:: html

   <hr>


The Hacken Open Air is best hacker camping, hosted by the `Stratum 0 <https://stratum0.org>`__.
Together with you, we want to tinker, discuss and be creative in the middle of the nature.

The location will once again be the Pfadfinderheim Welfenhof in Gifhorn, close to Brunswick. 
Address::

    Pfadfinderheim Welfenhof
    III. Koppelweg 6
    38518 Gifhorn

We are looking forward to see you!

Infrastructure
==============

The campsite has fixed indoor showers and toilets, and we build open-air showers.
In addition, we offer area-wide electricity, DECT and Wi-Fi, so you are perfectly equipped for hacking and chilling.
To chill there will probably be a pool with a sauna, as well as a directly adjacent campfire.
There you can enjoy the evening with drinks at the bar.

(H)Elves
========

As a community event, the HOA would not exist without the participation of the participants.
As every year, there are lots of things to do in preparation, during the event and during dismantling.

Art and decoration
==================

We love colorful lights and would be delighted if HOA25 is as beautiful as the previous HOA.
Anyone who has ideas for decorations or art installations is welcome to get involved (again).

We will be happy to answer any further questions about art and decoration at
`kontakt@hackenopenair.de <mailto:kontakt@hackenopenair.de>`__.

Food
====

There is a breakfast buffet every day between 09.00 and 12.00.
In the evening we will prepare dinner for everyone.

Breakfast and dinner during the event are included in the ticket price.
In between, we'll plan on offer some deep-fried food.

We have cutlery, dishes and cups that we wash ourselves, so there is no need to bring any of that.

Camping
=======

Per person there are about 12 m² available on the campground.
On average there are about 100 W of electricity per person available.


Day tickets
============

For all days (Tuesday to Friday) there are day tickets available at the box office.
This entitles you to visit the HOA from 09:00 until night.
One dinner is included.
The ticket does not entitle to overnight stay.


.. image:: faehnchen.jpeg
   :width: 600px
   :title: Flagpole with many colorful flags.

.. image:: leuchtebunt.jpeg
   :width: 600px
   :title: Many colorful light with tents at night.

.. image:: pool.jpeg
   :width: 600px
   :title: Swimming pool with unicorn, cargo bike and camp fire with colorful light at night (@shoragan CC-BY-SA).

